package com.transporeo.post.repositoryTest;

import com.transporeo.post.model.PackageModel;

import java.util.ArrayList;
import java.util.List;

public class RepositoryPackage {

    private List<PackageModel> listPackage = new ArrayList<>();

    RepositoryPackage() {

//        this.listPackage.add(new PackageModel("paczka1", PackageModel.Size.S,
//                12.2,"Nowak","Kowalski", RepositoryParcel.getListParcel().get(1), RepositoryParcel.getListParcel().get(2), PackageModel.State.PAC));
//        this.listPackage.add(new PackageModel("paczka2", PackageModel.Size.L,
//                15.0, "Jagielo", "Waza", RepositoryParcel.getListParcel().get(3),RepositoryParcel.getListParcel().get(4), PackageModel.State.IN));
//        this.listPackage.add(new PackageModel("paczka3", PackageModel.Size.M,
//                2.5, "Nowacek", "Kowalczyk", RepositoryParcel.getListParcel().get(5), RepositoryParcel.getListParcel().get(6), PackageModel.State.SEND));

    }

    public List<PackageModel> getListPackage() {
        return listPackage;
    }

    public void setListPackage(List<PackageModel> listPackage) {
        this.listPackage = listPackage;
    }


}
