package com.transporeo.post.model;

import java.util.UUID;

public class PackageModel {

    private UUID id;
    private  String name;
    private  Size size;
    private  int weight;
    private  String recipient;
    private  ParcelModel recipientParcel;
    private  String sender;
    private  ParcelModel senderParcel;
    private  State state;

    public PackageModel() {
    }

    public PackageModel(String name, Size size, int weight, String recipient, ParcelModel recipientParcel, String sender, ParcelModel senderParcel, State state) {
        this.id = UUID.randomUUID();
        this.name = name;
        this.size = size;
        this.weight = weight;
        this.recipient = recipient;
        this.recipientParcel = recipientParcel;
        this.sender = sender;
        this.senderParcel = senderParcel;
        this.state = state;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Size getSize() {
        return size;
    }

    public void setSize(Size size) {
        this.size = size;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public ParcelModel getSenderParcel() {
        return senderParcel;
    }

    public void setSenderParcel(ParcelModel senderParcel) {
        this.senderParcel = senderParcel;
    }

    public ParcelModel getRecipientParcel() {
        return recipientParcel;
    }

    public void setRecipientParcel(ParcelModel recipientParcel) {
        this.recipientParcel = recipientParcel;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return "PackageModel{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", size=" + size +
                ", weight=" + weight +
                ", recipient='" + recipient + '\'' +
                ", recipientParcel=" + recipientParcel +
                ", sender='" + sender + '\'' +
                ", senderParcel=" + senderParcel +
                ", state=" + state +
                '}';
    }

    public  enum Size {
        S("Small"),
        M("Medium"),
        L("Large"),
        XL("Extra Large");

        private String fullName;

        Size(String fullName) {
            this.fullName = fullName;
        }

        public String getFullName() {
            return fullName;
        }

    }

    public  enum State {
        PAC("Preparation package"),
        SEND("Sending to deliver"),
        DELIV("Deliver go to your parcel"),
        IN("Package in parcel");

        private String fullName;

        State(String fullName) {
            this.fullName = fullName;
        }

        public String getFullName() {
            return fullName;
        }

    }

}
