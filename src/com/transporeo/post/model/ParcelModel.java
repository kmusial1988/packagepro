package com.transporeo.post.model;

public class ParcelModel {

    private String id;
    private String name;
    private String address;

    public ParcelModel() {
    }

    public ParcelModel(String id, String name, String address) {
        this.id = id;
        this.name = name;
        this.address = address;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "ParcelModel{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
    //    public static class Adress{
//        private String street;
//        private String city;
//        private String postCode;
//
//
//        public Adress(String street, String city, String postCode) {
//            this.street = street;
//            this.city = city;
//            this.postCode = postCode;
//        }
//
//        public String getStreet() {
//            return street;
//        }
//
//        public void setStreet(String street) {
//            this.street = street;
//        }
//
//        public String getCity() {
//            return city;
//        }
//
//        public void setCity(String city) {
//            this.city = city;
//        }
//
//        public String getPostCode() {
//            return postCode;
//        }
//
//        public void setPostCode(String postCode) {
//            this.postCode = postCode;
//        }
//
//        @Override
//        public String toString() {
//            return "Adress{" +
//                    "street='" + street + '\'' +
//                    ", city='" + city + '\'' +
//                    ", postCode='" + postCode + '\'' +
//                    '}';
//        }
//    }

}
