package com.transporeo.post.service;

import com.transporeo.post.model.PackageModel;
import com.transporeo.post.model.ParcelModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static com.transporeo.post.Gui.showPackageMenu;
import static com.transporeo.post.Gui.showParcelMenu;


public class ServicePackage {

    private static final List<PackageModel> listPackage = new ArrayList<>();


    private static final Scanner scanner = new Scanner(System.in);


    public static void addPackage() {
//TODO przeskakuje??

        System.out.println("Write name :");
        String name = scanner.nextLine();
        System.out.println("Choice size S/M/L/XL :");
        PackageModel.Size size = PackageModel.Size.valueOf(scanner.next().toUpperCase());
        System.out.println("Write weight :");
        int weight = scanner.nextInt();
        System.out.println("Write name recipient :");
        String recipient = scanner.nextLine();
        System.out.println("Write recipient parcel name :");
        ParcelModel recipientParcel = ServiceParcel.getParcelByName(scanner.nextLine());
        System.out.println("Write name sender :");
        String sender = scanner.nextLine();
        System.out.println("Write sender parcel name :");
        ParcelModel senderParcel = ServiceParcel.getParcelByName(scanner.nextLine());
        System.out.println("Choice size PAC/SEND/DELIV/IN :");
        PackageModel.State state = PackageModel.State.valueOf(scanner.next().toUpperCase());

        listPackage.add(new PackageModel(name, size, weight, recipient, recipientParcel, sender, senderParcel, state));

        System.out.println();
        System.out.println("Package add !!!");
        System.out.println();
        System.out.println("----------------------");
        showPackageMenu();
    }

    public static void removePackage() {

        System.out.println("Write name package :");
        String test = scanner.nextLine();
String PackageToRemove = test.toLowerCase();
        for (int i = 0; i < listPackage.size(); i++) {

            if (listPackage.get(i).getName().toLowerCase().equals(PackageToRemove)) {
                listPackage.remove(i);
                System.out.println("Parcel " + PackageToRemove + " is remove");
            } else {
                System.out.println("!!!!!!!!!!!  Parcel is not exist, write correct words !!!!!!!!!!!!!!!!");
                showPackageMenu();
            }
        }

        System.out.println("----------------------");
        showPackageMenu();
    }

    public static void displayAllPackageInParcel() {
//TODO naprawic
        System.out.println("Enter the Name of parcel :");
        String parcelName = scanner.nextLine();
        String toCheck = parcelName.toLowerCase();

        for (int j = 0; j < listPackage.size(); j++) {
            if (listPackage.get(j).getSenderParcel().getName().contains(toCheck)) {
                for (int i = 0; i < listPackage.size(); i++) {
                    if (listPackage.get(i).getSenderParcel().getName().equals(toCheck)) {
                        if (i == 0) {
                            System.out.println("Package to shipping :");
                        }
                        System.out.println(listPackage.get(i));
                    } else {
                        if (i == 0) {
                            System.out.println("No packages for shipping !!!");
                        }
                    }
                }
                System.out.println("   -----------------------------   ");
            } else {
                System.out.println("Wrong name sendler parcel !!!");

            }
            if (listPackage.get(j).getRecipientParcel().getName().contains(toCheck)) {
                for (int i = 0; i < listPackage.size(); i++) {

                    if (listPackage.get(i).getRecipientParcel().getName().toLowerCase().equals(toCheck)) {
                        if (i == 0) {
                            System.out.println("Package to collection :");
                        }
                        System.out.println(listPackage.get(i));
                    } else {
                        if (i == 0) {
                            System.out.println("No packages for collection !!!");
                        }
                    }

                }
                System.out.println("   -----------------------------   ");
            } else {
                System.out.println("Wrong name recipient parcel !!!");

            }
//        for (int j = 0; j < listPackage.size(); j++) {
//
//            if (!listPackage.get(j).getRecipientParcel().getName().toLowerCase().equals(parcelName)
//                   || !listPackage.get(j).getSenderParcel().getName().toLowerCase().equals(parcelName)) {
//
//                System.out.println("Wrong name !!!");
//                showPackageMenu();
//            }
//        }
        }
    }

    public static void showAllPackage() {

        if (listPackage.size() == 0) {
            System.out.println("List is empty !!!");
            showParcelMenu();
        } else {

            for (PackageModel listToDisplay : listPackage) {
                System.out.println(listToDisplay);
            }
            System.out.println("----------------------");
            showPackageMenu();
        }
    }

    public static void editPackage() {

        System.out.println("Write parameter package to Edit: ");
        String test = scanner.nextLine();
        String parcelToRemove = test.toLowerCase();



        for (int i = 0; i < listPackage.size(); i++) {

            if (listPackage.get(i).getName().toLowerCase().equals(parcelToRemove)) {
                System.out.println("Write new Name to replace: ");
                String name = scanner.nextLine();

                listPackage.get(i).setName(name);
                System.out.println("Parcel " + parcelToRemove + " is edit to " + name);

            } else if (listPackage.get(i).getRecipient().toLowerCase().equals(parcelToRemove)) {
                System.out.println("Write new recipient to replace: ");
                String recipient = scanner.nextLine();

                listPackage.get(i).setRecipient(recipient);

                System.out.println("Parcel " + parcelToRemove + " is edit to" + recipient);
            } else if (listPackage.get(i).getSender().toLowerCase().equals(parcelToRemove)) {
                System.out.println("Write new sender to replace: ");
                String sender = scanner.nextLine();

                listPackage.get(i).setSender(sender);

                System.out.println("Parcel " + parcelToRemove + " is edit to " + sender);
            } else if (listPackage.get(i).getWeight() == Integer.parseInt(parcelToRemove)) {
                System.out.println("Write new weight to replace: ");

                int weight = scanner.nextInt();

                listPackage.get(i).setWeight(weight);

                System.out.println("Parcel " + parcelToRemove + " is edit to " + weight);
            } else if (listPackage.get(i).getSize().equals(PackageModel.Size.valueOf(parcelToRemove))) {

                System.out.println("Write new Size to replace: ");

                PackageModel.Size size = PackageModel.Size.valueOf(scanner.next().toUpperCase());

                listPackage.get(i).setSize(size);

                System.out.println("Parcel " + parcelToRemove + " is edit to " + size.getFullName());
            } else if (listPackage.get(i).getState().equals(PackageModel.State.valueOf(parcelToRemove))) {

                System.out.println("Write new state to replace: ");

                PackageModel.State state = PackageModel.State.valueOf(scanner.next().toUpperCase());

                listPackage.get(i).setState(state);

                System.out.println("Parcel " + parcelToRemove + " is edit to " + state.getFullName());

            } else if (listPackage.get(i).getSenderParcel().getName().equals(parcelToRemove)) {

                System.out.println("Write new parcel name to replace: ");

                ParcelModel senderParcel = ServiceParcel.getParcelByName(scanner.nextLine());

                listPackage.get(i).setSenderParcel(senderParcel);

                System.out.println("Parcel " + parcelToRemove + " is edit to " + senderParcel.getName());

            } else if (listPackage.get(i).getRecipientParcel().getName().equals(parcelToRemove)) {

                System.out.println("Write new parcel name to replace: ");

                ParcelModel recipientParcel = ServiceParcel.getParcelByName(scanner.nextLine());

                listPackage.get(i).setSenderParcel(recipientParcel);

                System.out.println("Parcel " + parcelToRemove + " is edit to " + recipientParcel.getName());

            } else {

                System.out.println("!!!!!!!!!!!  Package is not exist, write correct words !!!!!!!!!!!!!!!!");
                showPackageMenu();
            }
        }
        System.out.println("----------------------");
        showPackageMenu();
    }


}
