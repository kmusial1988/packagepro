package com.transporeo.post.service;

import com.transporeo.post.model.ParcelModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.transporeo.post.Gui.showParcelMenu;

public class ServiceParcel {


    private static final List<ParcelModel> listParcel = new ArrayList<>();


    private static final Scanner scanner = new Scanner(System.in);


    public static void displayAllParcel() {
        if (listParcel.size() == 0) {
            System.out.println("List is empty !!!");
            showParcelMenu();
        } else {

            for (ParcelModel listToDisplay : listParcel) {
                System.out.println(listToDisplay);
            }
            System.out.println("----------------------");
            showParcelMenu();
        }
    }

    public static void displayParcelByName() {
        System.out.println("Write parcel City :");
        String parcelCity = scanner.nextLine();

        String toCheck = parcelCity.toLowerCase();

        for (int i = 0; i < listParcel.size(); i++) {

            if (listParcel.get(i).getAddress().toLowerCase().contains(toCheck)) {

                System.out.println(listParcel.get(i));
            }
        }
        System.out.println("----------------------");
        showParcelMenu();
    }

    public static ParcelModel getParcelByName(String name) {


        ParcelModel result = new ParcelModel();

        String toCheck = name.toLowerCase();

        for (int i = 0; i < listParcel.size(); i++) {

            if (listParcel.get(i).getName().toLowerCase().equals(toCheck)) {

                result.setName(listParcel.get(i).getName());
                result.setId(listParcel.get(i).getId());
                result.setAddress(listParcel.get(i).getAddress());
            }
        }
        return result;
    }



    public static void addParcel() {

        System.out.println("Write id :");
        String id = scanner.nextLine();

//        Pattern idPatern = Pattern.compile(".*[0-9]+.*");
//        Matcher idMatcher = idPatern.matcher(id);
//        idMatcher.matches();

        System.out.println("Write name :");
        String name = scanner.nextLine();
        System.out.println("Write address : street, city, postal code :");
        String address = scanner.nextLine();


        listParcel.add(new ParcelModel(id, name, address));
        System.out.println("----------------------");
        showParcelMenu();

    }


    public static void removeParcel() {
        System.out.println("Write id or name or address parcel to remove:");

        String test = scanner.nextLine();
        String parcelToRemove = test.toLowerCase();


        for (int i = 0; i < listParcel.size(); i++) {

            if (listParcel.get(i).getName().toLowerCase().equals(parcelToRemove)) {

                listParcel.remove(i);
                System.out.println("Parcel " + parcelToRemove + " is remove");
            } else if (listParcel.get(i).getId().toLowerCase().equals(parcelToRemove)) {

                listParcel.remove(i);
                System.out.println("Parcel " + parcelToRemove + " is remove");
            } else if (listParcel.get(i).getAddress().toLowerCase().equals(parcelToRemove)) {

                listParcel.remove(i);
                System.out.println("Parcel " + parcelToRemove + " is remove");
            } else {
                System.out.println("!!!!!!!!!!!  Parcel is not exist, write correct words !!!!!!!!!!!!!!!!");
                showParcelMenu();
            }
        }
        System.out.println("----------------------");
        showParcelMenu();
    }

    public static void editParcel() {
        System.out.println("Write id or name or address parcel to Edit: ");
        String test = scanner.nextLine();
        String dataToEdit = test.toLowerCase();

        for (int i = 0; i < listParcel.size(); i++) {


            if (listParcel.get(i).getName().toLowerCase().equals(dataToEdit)) {
                System.out.println("Write new Name to replace: ");

                String name = scanner.nextLine();

                listParcel.get(i).setName(name);
                System.out.println("Parcel " + dataToEdit + " is edit to " + name);

            }
            if (listParcel.get(i).getId().toLowerCase().equals(dataToEdit)) {
                System.out.println("Write new Id to replace: ");
                String Id = scanner.nextLine();

                listParcel.get(i).setId(Id);

                System.out.println("Parcel " + dataToEdit + " is edit to " + Id);
            }
            if (listParcel.get(i).getAddress().toLowerCase().contains(dataToEdit)) {
                System.out.println("Write new Address to replace: ");
                String address = scanner.nextLine();

                listParcel.get(i).setAddress(address);

                System.out.println("Parcel " + dataToEdit + " is edit to " + address);
            }
            if(!listParcel.get(i).getName().contains(dataToEdit) || !listParcel.get(i).getAddress().contains(dataToEdit) || !listParcel.get(i).getId().contains(dataToEdit)){
                System.out.println("!!!!!!!!!!!  Parcel is not exist, write correct words !!!!!!!!!!!!!!!!");
                showParcelMenu();
            }
        }
        System.out.println("----------------------");
        showParcelMenu();
    }


}

