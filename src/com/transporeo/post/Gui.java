package com.transporeo.post;

import java.util.Scanner;

import static com.transporeo.post.service.ServicePackage.*;
import static com.transporeo.post.service.ServiceParcel.*;

public class Gui {

    private static final Scanner scanner = new Scanner(System.in);

    public static void showMainMenu() {
        //TODO walidacje
        String choose1;

        do{

            System.out.println("1. Package");
            System.out.println("2. Parcel");
            System.out.println("3. Exit");

            choose1 = scanner.nextLine();


            switch (choose1) {
                case "1":
                    showPackageMenu();
                    break;
                case "2":
                    showParcelMenu();
                    break;
                case "3":
                    break;
                default:
                    System.out.println("Wrong choice !!");
                    break;
            }

        }while (!choose1.equals("3"));

    }

    public static void showParcelMenu() {


            System.out.println("1. Add Parcel");
            System.out.println("2. Remove Parcel");
            System.out.println("3. Display All Parcel");
            System.out.println("4. Display Parcel By City");
            System.out.println("5. Edit Parcel");
            System.out.println("6. Back");

            String chooise2 = scanner.nextLine();

            switch (chooise2) {
                case "1":
                    addParcel();
                    break;
                case "2":
                    removeParcel();
                    break;
                case "3":
                    displayAllParcel();
                    break;
                case "4":

                    displayParcelByName();
                    break;
                case "5":
                    editParcel();
                    break;
                case "6":
                    showMainMenu();
                    break;
                default:
                    System.out.println("Wrong choice !!");
                    break;
            }


    }

    public static void showPackageMenu() {


            System.out.println("1. Add Package");
            System.out.println("2. Remove Package");
            System.out.println("3. Display All Package");
         //   System.out.println("4. Display All Package in parcel by name parcel ");
            System.out.println("5. Edit package details");
            System.out.println("6. Back");

            String choose3 = scanner.nextLine();

            switch (choose3) {
                case "1":
                    addPackage();
                    break;
                case "2":
                    removePackage();
                    break;
                case "3":
                    showAllPackage();
                    break;
                case "4":
                    displayAllPackageInParcel();
                    break;
                case "5":
                    editPackage();
                    break;
                case "6":
                    showMainMenu();
                    break;
                default:
                    System.out.println("Wrong choice !!");
                    break;
            }

    }


}
